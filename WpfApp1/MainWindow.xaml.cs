﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DispatcherTimer timer1 = new DispatcherTimer();
        
        Timer timer = new Timer(1000);
        private int i = 0;
        public MainWindow()
        {
            InitializeComponent();
            timer.Elapsed +=  Update;
            timer1.Interval = TimeSpan.FromSeconds(1000);
            //change
            //blabla
            
        }

        private async void Update(object sender, ElapsedEventArgs e)
        {
            i++;
            Dispatcher.Invoke(new Action(() =>
            {
                Txt.Text = i.ToString();
            }));

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            timer.Start();
        }
    }
}
